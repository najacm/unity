﻿using UnityEngine;
using System.Collections;

using System.Collections.Generic;

using OpenCVForUnity;

namespace OpenCVFaceTracker
{

		public class FaceTrackerParams
		{

				public List<OpenCVForUnity.Size> ssize;
				public bool robust;
				public int itol;
				public double ftol;

				public FaceTrackerParams (bool robust = true, int itol = 20, double ftol = 1e-3, List<OpenCVForUnity.Size> ssize = null)
				{
						if (ssize == null) {
								this.ssize = new List<Size> ();
								this.ssize.Add (new OpenCVForUnity.Size (21, 21));
								this.ssize.Add (new OpenCVForUnity.Size (11, 11));
								this.ssize.Add (new OpenCVForUnity.Size (5, 5));
						} else {
								this.ssize = ssize;
						}

						this.robust = robust;
						this.itol = itol;
						this.ftol = ftol;
				}
		}
}
