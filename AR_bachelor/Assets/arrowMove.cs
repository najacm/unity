﻿using UnityEngine;
using System.Collections;

public class arrowMove : MonoBehaviour {

	public GameObject gameobject;
	public GameObject rotationPoint;

	Vector3 rotationVektor = Vector3.forward;
	Vector3 rotationVektorBack = Vector3.back;

	//vectors for setting the axis to rotate around (bottom of the ax)
	Vector3 bundPos = new Vector3(4f,0f,0f);
	Vector3 pos;
	Vector3 axisPoint;


	// Use this for initialization
	void Start () {

		pos = transform.position;
		axisPoint = pos - bundPos;

	}

	// Update is called once per frame
	void Update () {

		if (Timer.i == 1) {
			transform.RotateAround(axisPoint, rotationVektor, 40 * Time.deltaTime);
		} else if (Timer.i == -1) {
			transform.RotateAround(axisPoint, rotationVektorBack, 40 * Time.deltaTime);
		}

	}
}
